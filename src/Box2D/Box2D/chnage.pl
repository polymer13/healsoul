use strict;
use warnings;

my $file;
my $fh;
my $line = "";

foreach $file (@ARGV) {
    open FH, '<', $file;
    my $data = "";
    while ( <FH>) {
        $line = $_;
        if ( $line =~ /^#include\s+"(.+)"/ ) {
            $line = "#include  <Box2D/$1>\n";
        }

        $data = $data . $line ;
    }
    close FH;
    open WR, ">", $file;
    print WR  $data;
    
}
