#include "mathem.hxx"


float math::angle_from_origin( Vec2f f )
{
  return std::acos ( f.y / f.x ); 
}

float math::magnitude ( Vec2f v )
{
  float x = v.x * v.x;
  float y = v.y * v.y;
  return std::sqrt( x + y );
}

