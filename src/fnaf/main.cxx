#include <healsoul/core/core.hxx>
#include <healsoul/core/updates.hxx>
#include <healsoul/core/timers.hxx>
#include <healsoul/core/curve.hxx>
#include <healsoul/core/grid.hxx>
#include <SFML/Audio.hpp>
#include <iostream>
#include <cmath>

using namespace core;
using math::Vec2f;
using math::Vec2i;

sf::String core::default_font_path ("assets/fonts/rabbit.otf");
sf::Font core::default_font;
int core::default_font_size = 15;
std::shared_ptr<sf::RenderWindow> core::default_window;

static sf::RectangleShape rect ( sf::Vector2f( 100, 100 ) );
static sf::Texture background_texture;
static sf::RectangleShape background ; 
static sf::RectangleShape r_green; 
static sf::SoundBuffer scream;
static sf::Music bm; // background music
static sf::VideoMode resolution; 


void prepare ()
{
  background_texture.loadFromFile("assets/images/background01.jpg");
  background.setTexture(  &background_texture );
  background.setSize( math::Vec2f( resolution.width, resolution.height ) );
  background.setPosition( Vec2f( 0, 0 ) );
  if ( !scream.loadFromFile("assets/sounds/scream01.wav") ) {
    std::cerr << "Could not load scream!\n";
    exit (1);
  }
  if (! bm.openFromFile("assets/sounds/jesutod.ogg") ) {
    std::cerr << "Could not load music file!\n";
    exit( 1 );
  }

}

static Vec2f poss ( 100, 0 );
static size_t head_count = 20;

static LinearStep<float> stepy ( 0, 0, 1.0, 0.0005 );
static LinearStep<float> stepx ( 0, 0, 1, 0.008 );
Vec2f base ( 0, 0 );
static geom::Curve curve ( base, Vec2f( base.x + 250, base.y + 150 ), Vec2f( base.x, base.y + 300 ) );
static geom::Curve curve2 ( Vec2f( resolution.width - 100 , resolution.height - 100 ), Vec2f( 400, 0 ), Vec2f( 400, 400 ) );
static geom::CurveIter curve_iter( curve, 0.0005 );
static geom::CurveIter curve_iter2( curve2, 1 / 2000 );

core::App& app = core::App::GetInstance();
gfx::Line line1 ( sf::Color::White, Vec2f( 100,30 ), Vec2f ( 200, 50 ) );
gfx::Grid grid1 ( sf::Color::Green, Vec2f( 10, 10 ), Vec2f( 100, 100), 10 );

void prepare_falls_update()
{
  r_green.setFillColor( sf::Color::Red );
  r_green.setSize( Vec2f( 5, 5 ) );
  grid1.SetSize( Vec2f(resolution.width, resolution.height) );
  Vec2f off ( 400, 300 );
  Vec2f tmp ( 0, 0);
  float mag = 0;
  app.on_mouse_down.push_back( [&]( const Vec2f& mp ){
    });
  app.on_mouse_down.push_back( [&]( const Vec2f& mp ){
    });
  
}



void falls_update()
{
  Vec2f pos (resolution.width/2-100,100);
  Vec2f size ( 203, 1 );
  sf::RectangleShape tmp;
  tmp.setSize( size );
  line1.SetThickness( 5 );
  line1.DrawToWindow( app.sfml_main_window );
  grid1.DrawToWindow( app.sfml_main_window );
  tmp.setFillColor ( sf::Color::Blue );
  for ( int x = 0; x < 4000 ; x++ ) {
    pos = *(curve_iter++);
    pos.y += 100;
    Vec2f right = pos;
    Vec2f left = pos;
    left.x = resolution.width / 2 - pos.x - 100;
    right.x = resolution.width / 2 + pos.x + 100;
    // pos2 = *(curve_iter2++);

    pos.x = resolution.width / 2 - pos.x;
    left.x += 5;
    right.x += 5;

    if ( x > 1 ) {
      size.x = right.x - (left.x);
      pos.x = pos.x - 100 + 5 ;
      tmp.setSize( size );
      tmp.setPosition( pos );
    }

    // r_green.setPosition( pos2 );
    // app.sfml_main_window.draw( r_green );
    app.sfml_main_window->draw( tmp );
    r_green.setPosition( left );
    app.sfml_main_window->draw( r_green );
    r_green.setPosition( right );
    app.sfml_main_window->draw( r_green );
  }
  pos = Vec2f(resolution.width/2-95,100);
  size = Vec2f( 205, 5 );
  tmp.setFillColor( sf::Color::Red );
  tmp.setSize( size );
  tmp.setPosition( pos);
  app.sfml_main_window->draw( tmp );
  pos.y += 300;
  tmp.setPosition( pos);
  app.sfml_main_window->draw( tmp );
}

int main(int argc, char *argv[])
{
  resolution = sf::VideoMode( 800, 600);//sf::VideoMode::getDesktopMode();
  app.Init( Vec2i( resolution.width , resolution.height ), "Hello, there!");
  Vec2i res = app.GetResolution();
  using namespace gui;
  auto btn1 = std::make_shared<gui::Button> ( "Play!", sf::Color::Blue,
                                         Vec2f ( res.x/2-100, res.y/2-25), Vec2f( 200, 30));
  auto btn2 = std::make_shared<gui::Button> ( "Run Away!", sf::Color::Red,
                                         Vec2f ( res.x/2-100, res.y/2+25), Vec2f( 200, 30));
  auto input1 = std::make_shared<gui::InputField> ( "Field1", sf::Color::Magenta,
                                         Vec2f ( 10, res.y-50), Vec2f( res.x-10, 20));
  auto label1 = std::make_shared<Label>( "Hello, there!",
                                         sf::Color::Red,
                                         Vec2f( 0, 0 ), Vec2f( 300, 25 ));


  prepare();
  btn2->click_events.push_back( [&] () {
      app.Stop();
    });

  sf::Sound sound; sound.setBuffer( scream );
  sound.setLoop( false );
  std::cout << "Starting program: " << argv[0] << std::endl;

  app.widgets.push_back( btn1 );
  app.widgets.push_back( btn2 );
  app.widgets.push_back( input1 );
  app.widgets.push_back( label1 );
  // bm.play();


  input1->SetTextReciever( [&]( sf::String s ) {
      label1->SetText( s );
    });

  Update main_update ( [&](){
      default_window->draw( background );
      if( rand() % 6000 == 666 )
        sound.play();
      app.widgets[0]->SetXY( Vec2f( res.x/2-125 + stepx.Next() * 50 , res.y/2-20));
      app.widgets[1]->SetXY( Vec2f( res.x/2-75 - stepx.Next() * 50 , res.y/2+20));
    } );

  Update game_update( &falls_update );

  btn1->click_events.push_back ( [&](){
      btn2->SetXY( 10, res.y - 50 );
      btn2->SetSize( 100, 35);
      btn2->Refresh();
      btn1->SetActive( false );
      prepare_falls_update();
      app.updates[0] = &falls_update;
    });

  app.updates.push_back( main_update );
  std::cout << "Number of updates: " << app.updates.size() << std::endl;
  return app.Start( 120 );
}
