#include <iostream>
#include <fstream>
#include <iomanip>

int main ( int argc, char** argv )
{
  if ( argc < 4 ) {
    std::cerr << "Not enought arguments!\n";
    exit(1);
  }
  std::string fn_in ( argv[1] );
  std::string fn_out ( argv[2] );
  std::string buffer_name ( argv[3] );
  std::cout << "File IN: " << fn_in  << std::endl << "Buffer: " << buffer_name << std::endl
            << "File OUT: " << fn_out << std::endl;
  std::string yes;
  std::cout << "OK! [y/n]: ";
  std::cin >> yes;
  if( yes != "y" ) {
    std::cout << "NO!\n";
    return 0;
  }
  std::cout << "YES!\n";

  std::fstream fin ( fn_in, std::ios::in );
  std::fstream fout ( fn_out, std::ios::out );

  if ( !fin || !fin.is_open() || fin.bad() ) {
    std::cerr << "Could not open input file\n!";
    return 1;
  }

  if ( !fout || fout.bad() ){
    std::cerr << "Could not open output file\n!";
    return 1;
  }

  size_t count = 0;
  char buffer[100];
  fout << "char data_buffer_" << buffer_name << " [] = {\n\t" ;
  while( !fin.eof() && !fin.bad() ) {
    fin.read( buffer, 100 );
    size_t count = fin.gcount();
    for ( size_t x = 0; x < count; x++ ) {
      if( x % 10 == 0 )
        fout << "\n\t";
      int c = static_cast<int>( buffer[x] ) & 0xff;
      fout << "0x" << std::hex << c << ", ";
      if( c < 0x10 )
        fout << " ";
    }
    fout << std::endl;
  }

  fout << "\n};\n";
  fin.close();
  fout.close();
  return 0;
}
