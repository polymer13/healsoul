#include "gl_includes.hxx"

#include "shader.hxx"
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include "attribute.hxx"

using namespace glo;


ShaderProgram::ShaderProgram( std::string vertex_file_path,
                              std::string fragment_file_path )
{

	// Create the shaders

  GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
  std::stringstream msg;

	// Read the Vertex Shader code from the file
	std::string vertex_shader_code;
	std::ifstream vertex_shader_stream(vertex_file_path.c_str(), std::ios::in);
	if(vertex_shader_stream.is_open()){
		std::string Line = "";
		while(getline(vertex_shader_stream, Line))
			vertex_shader_code += "\n" + Line;
		vertex_shader_stream.close();
	}else{
    msg << "Could not open vertex shader file!\n"
        << "Check if you are in the correct directory !";
    throw std::runtime_error( msg.str() );
	}

	// Read the Fragment Shader code from the file
	std::string fragment_shader_code;
	std::ifstream fragment_shader_stream(fragment_file_path.c_str(), std::ios::in);
	if(fragment_shader_stream.is_open()){
		std::string Line = "";
		while(getline(fragment_shader_stream, Line))
			fragment_shader_code += "\n" + Line;
		fragment_shader_stream.close();
	}

	GLint result = GL_FALSE;
	int InfoLogLength;


	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path.c_str());
	char const * VertexSourcePointer = vertex_shader_code.c_str();
	glShaderSource(vertex_shader_id, 1, &VertexSourcePointer , NULL);
	glCompileShader(vertex_shader_id);

	// Check Vertex Shader
	glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertex_shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> vertex_shader_error_message(InfoLogLength+1);
		glGetShaderInfoLog(vertex_shader_id, InfoLogLength, NULL,
                       &vertex_shader_error_message[0]);
		throw std::runtime_error( &vertex_shader_error_message[0] );
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path.c_str());
	char const * fragment_source_pointer = fragment_shader_code.c_str();
	glShaderSource(fragment_shader_id, 1, &fragment_source_pointer , NULL);
	glCompileShader(fragment_shader_id);

	// Check Fragment Shader
	glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragment_shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> fragment_shader_error_message(InfoLogLength+1);
		glGetShaderInfoLog(fragment_shader_id, InfoLogLength, NULL, &fragment_shader_error_message[0]);
		throw std::runtime_error( &fragment_shader_error_message[0] );
	}



	// Link the program
	printf("Linking program\n");
  program_id = glCreateProgram();
	glAttachShader(program_id, vertex_shader_id);
	glAttachShader(program_id, fragment_shader_id);
	glLinkProgram(program_id);

	// Check the program
	glGetProgramiv(program_id, GL_LINK_STATUS, &result);
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(program_id, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		throw std::runtime_error( &ProgramErrorMessage[0] );
	}

	
	glDetachShader(program_id, vertex_shader_id);
	glDetachShader(program_id, fragment_shader_id);
	
	glDeleteShader(vertex_shader_id);
	glDeleteShader(fragment_shader_id);
  std::cout << "program_id: " << program_id << std::endl;
	
}


GLuint ShaderProgram::GetProgramID() const
{
  return program_id;
}


Attribute ShaderProgram::GetAttribute( std::string const& name )
{
  Attribute attr;
  GLuint attr_id = glGetAttribLocation( program_id, name.c_str() );
  attr.location_id = attr_id;
  attr.name =  name;
  return attr;
}


Uniform ShaderProgram::GetUniform( std::string const& name )
{
  GLuint unif_id = glGetUniformLocation( program_id, name.c_str() );
  return Uniform( name, unif_id );
}


void ShaderProgram::Use() const
{
  glUseProgram( program_id );
}


