set( ROOT_DIR "${PROJECT_SOURCE_DIR}")
set( INCLUDE_DIR "${ROOT_DIR}/src" )
if ( WIN32 )
   set( GCC_COVERAGE_COMPILE_FLAGS  "-mwindows" )
   set( GCC_COVERAGE_LINK_FLAGS "-mwindows" )
endif()


find_package( OpenGL REQUIRED )
find_package( GLEW REQUIRED )
find_package( Boost 1.61.0 REQUIRED ) 

if( ${Boost_INCLUDE_DIR} )
    message("PLEASE SETUP Boost_INCLUDE_DIR")
endif()

if( OpenGL_FOUND ) 
  if( GLEW_FOUND ) 
  aux_source_directory( ${CMAKE_CURRENT_SOURCE_DIR} SOURCE_FILES )
  # include_directories( ${INCLUDE_DIR} "${ROOT_DIR}/Box2D"  )
  include_directories( ${INCLUDE_DIR} ${Boost_INCLUDE_DIRS} ${GLEW_INCLUDE_DIRS} ${SFML_INCLUDE_DIR} )
  add_executable ( glo ${SOURCE_FILES})
  target_link_libraries( glo PUBLIC core Box2D ${OPENGL_LIBRARY} ${OPENGL_gl_LIBRARIES} ${OPENGL_gl_LIBRARIES} ${GLEW_LIBRARIES} )
endif( GLEW_FOUND )
else()
  message("OpenGL NOT FOUND!")
endif()

