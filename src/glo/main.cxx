#include <healsoul/core/core.hxx>
#include <healsoul/core/mathem.hxx>
#include <healsoul/core/image.hxx>
#include <healsoul/core/layout.hxx>

#include "shader.hxx"
#include "buffer.hxx"
#include "attribute.hxx"
#include "obj_import.hxx"
#include <iostream>
#include <exception>
#include "data.h"
#include "texture.hxx"
#include <memory>
#include <ostream>

/*
  TODO: AddClickEvent  -> AddClickHandler
  do the same with similar
*/

using heal::core::App;
using namespace heal::gui;
using namespace glo;

ShaderProgramPtr main_shader;

static std::shared_ptr<bool> spin_the_cube; 

#define IMPL_SHARED_PTR_CTOR(name,item,td/*typedef*/) \
  using td = std::shared_ptr<item> ;            \
  template<typename... Args>                    \
  std::shared_ptr<item> name ( Args... args )   \
  { return std::make_shared<item>( args... ); } \
  

std::ostream& operator<<( std::ostream& out, glm::vec3 const& v )
{
  out << "vec3{ " << v.x << ", " << v.y << ", " << v.z << " }" ; 
 return out;
}

std::ostream& operator<<( std::ostream& out, heal::math::Vec2u const& v )
{
  out << "vec2 uint{ " << v.x << ", " << v.y << " }" ; 
  return out;
}


std::ostream& operator<<( std::ostream& out, glm::vec4 const& v )
{
  out << "vec4 {" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << " }"; 
  return out;
}



std::ostream& operator<<( std::ostream& out, glm::mat3 const& m )
{
  out << "mat3 {\n\t";
  for( int i = 0; i < 3; i++ ){
    out << m[i] << "\n\t";
  }
  out << "\r}\n";
  return out;
}

std::ostream& operator<<( std::ostream& out, glm::mat4 const& m )
{
  out << "mat3 {\n\t";
  for( int i = 0; i < 4; i++ ){
    out << m[i] << "\n\t";
  }
  out << "\r}\n";
  return out;
}



IMPL_SHARED_PTR_CTOR(NewShaderProgram,ShaderProgram,ShaderProgramPtr)

template<typename T, typename B>
void many( T oper, B item )
{
  oper(item);
}

template<typename Oper, typename T, typename... Args>
void many(  Oper oper, T item, Args... args )
{
  oper( item );
  many(oper, args...);
}


void setup_app()
{
  
  using heal::math::Vec2f;
  using heal::core::AppSettings;

  spin_the_cube = std::make_shared<bool>( false );

  heal::core::AppSettings app_setup; 
  app_setup. single_window = true;
  app_setup. opengl_ready = true;
  app_setup. depth_bits = 24;
  app_setup. stencil_bits = 8;
  app_setup. antialiasing_level = 4;
  app_setup. opengl_major_version = 2;
  app_setup. opengl_minor_version = 1 ;


  App& app = App::GetInstance();
  heal::math::Vec2i window_size( 640, 480 );
  app.Init( window_size, "GLO", app_setup );


  GLuint glew_ok = glewInit();

  if( glew_ok != GLEW_OK ){
    throw std::runtime_error( "GLEW COULD NOT INITIALISE!" );
  }

  using heal::math::Vec2f;

  auto lay1 = app.NewWidget<heal::gui::VLayout>( Vec2f( 20,20 ), Vec2f(200,300), 5);
  auto btn1 = app.NewButton( "EXIT" ); 
  auto btn2 = app.NewButton( "Spin" );
  auto lbl1 = app.NewLabel( "SPINNING: OFF" ); 
  auto lbl2 = app.NewLabel( "This is cool!" ); 
  auto inp1 = app.NewInputField("Souce");

  // lay1->AddWidget( btn1 );
  // lay1->AddWidget( btn2 );
  // lay1->AddWidget( lbl1 );

  many( [=]( decltype(btn1) item ) 
        {
          lay1->AddWidget( item );
        },
    btn1, btn2 
    );

  many( [=]( decltype(lbl1) item ) 
        {
          lay1->AddWidget( item );
        },
    lbl1, lbl2 
    );

  lay1->AddWidget( inp1 );

  main_shader = NewShaderProgram( "assets/shaders/basic_vertex.glsl",
                                   "assets/shaders/basic_frag.glsl"   );

  btn1->SetClickAction([=]( heal::gui::ClickInfo ){
      auto& a = App::GetInstance();
      a.Stop();
    });

  btn2->SetClickAction([=]( heal::gui::ClickInfo ){
      *spin_the_cube = !(*spin_the_cube);
      lbl1->SetText( *spin_the_cube ?
                     "SPINNING: ON" : "SPINNING: OFF" );
    });

  
}

int main ( void )
{
  using heal::gfx::Image;
  try{
    setup_app();
    App& app = App::GetInstance();


    glo::ObjData model_data;
    glo::load_obj_file( "assets/test01.obj", model_data );

    std::cout << "Model vert count: " << model_data.verticies.size() << std::endl;

    // Enable depth testing 
    Buffer buff( Buffer::Type::ARRAY_BUFFER,  Buffer::Mode::STATIC_DRAW );
    buff.CopyFrom( 0, model_data.verticies );
    buff.Refresh();

    Buffer uv_buff( Buffer::Type::ARRAY_BUFFER, Buffer::Mode::STATIC_DRAW );
    uv_buff.CopyFrom( 0, model_data.uvs );
    uv_buff.Refresh();

    // for(size_t i = 0; i < model_data.normals.size(); i+=3){
    //   std::cout << "NORMAL: "
    //             << "| X: " << model_data.normals[i]
    //             << "| Y: " << model_data.normals[i+1]
    //             << "| Z: " << model_data.normals[i+2]
    //             << "|\n";
    // }

    Buffer normal_buff( Buffer::Type::ARRAY_BUFFER, Buffer::Mode::STATIC_DRAW );
    normal_buff.CopyFrom( 0, model_data.normals );
    normal_buff.Refresh();

    std::cout << "uv_count: " << uv_buff.GetSize(DataType::FLOAT) << std::endl;

    heal::gfx::Image texture;
    heal::gfx::Image normal_map;
    texture.LoadFromFile("assets/test01.png");
    normal_map.LoadFromFile("assets/test01_nm.png");
    heal::math::Vec2u texture_size = texture.GetSize();
    heal::math::Vec2u normal_map_size = normal_map.GetSize();
    Sampler2D texture_buff( texture_size.x, texture_size.y );
    Sampler2D normal_map_buff( normal_map_size.x, normal_map_size.y );
    std::cout << normal_map_size << std::endl;

    texture_buff.CopyFrom( 0, texture.GetByteCount(), texture.GetPixelsPtr() );
    texture_buff.Refresh();
    normal_map_buff.CopyFrom( 0, normal_map.GetByteCount(), normal_map.GetPixelsPtr() );
    normal_map_buff.Refresh();

    Attribute attr = main_shader->GetAttribute( "vertex_position_modelspace" );
    Attribute uv_attr = main_shader->GetAttribute( "vertex_uv" );
    Attribute normal_attr = main_shader->GetAttribute( "vertex_normal" );

    Uniform texture_uniform = main_shader->GetUniform("texture_sampler");
    Uniform normal_map_uniform = main_shader->GetUniform("normal_map_uniform");
    Uniform view_mat = main_shader->GetUniform("view_mat");
    Uniform proj_mat = main_shader->GetUniform("proj_mat");
    Uniform model_mat = main_shader->GetUniform("model_mat");
    Uniform normal_mat = main_shader->GetUniform("normal_mat");
    Uniform view_pos = main_shader->GetUniform("view_pos"); // vec3 

    Mat4x4 MVP ( 1.0 );
    Mat4x4 m4_model ( 1.0 );
    glm::mat3 m3_normal;

    glm::mat4 m4_projection = glm::perspective( 45.0f, 16.0f / 9.0f, 0.2f, 100.0f );

    glm::vec3 view_position (4,3,3);
    glm::mat4 m4_view       = glm::lookAt( view_position, 
                                           glm::vec3(0,0,0), 
                                           glm::vec3(0,1,0) );

    m3_normal = glm::mat3( glm::transpose( glm::inverse( m4_model ) ) ); 

    MVP = m4_projection * m4_view * MVP;

    MVP =  glm::scale( MVP, Vec3f( 1.0f, 1.0f, 1.0f ) );
    // MVP =  glm::rotate( MVP, 90.0f, Vec3f( 0.0f, 0.0f, 1.0f  ) );

    glClearColor( 1.0, 0.0, 0.1, 1.0 );

    glDepthMask( GL_TRUE );
    glClearDepth( 1.0f );
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LESS );

    vec3 view_position_original = view_position;

    app.AddUpdate( [&]{ 

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        main_shader->Use();
        // w = window_w
        // h = (window_w / 16.0) * 9.0
        // x = 0.0
        // y = (window_h - h)/ 2.0

        heal::math::Vec2u window_size = app.GetMainWindowSize();
        heal::math::Vec2i viewport_size ( window_size.x, (int)((float)window_size.y * (9.0f/16.0f)));
        glViewport( 0, (window_size.y-viewport_size.y)/2, viewport_size.x, viewport_size.y);

        texture_uniform.Set( 0 ); // Texture is in Unit 0
        glo_active_texture( 0 ) ; // Unit 0 -> active
        texture_buff.Bind();

        normal_map_uniform.Set( 1 );
        glo_active_texture( 1 ) ; // Unit 1 -> active
        normal_map_buff.Bind();

        if( *spin_the_cube )
          m4_model = glm::rotate( m4_model , 0.005f, Vec3f( 1.0f, 1.0f, 0.0f ) );

        view_position = glm::mat3(m4_view) * view_position_original;

        m3_normal = glm::mat3( glm::transpose( glm::inverse( m4_model ) ) ); 

        model_mat.Set( m4_model, false );
        proj_mat.Set( m4_projection, false );
        view_mat.Set( m4_view, false );
        normal_mat.Set( m3_normal, false );
        view_pos.Set( view_position );

        attr.Enable();

        buff.Bind();
        attr.Configure( 3, glo::DataType::FLOAT,
                        false, 0, nullptr );

        uv_attr.Enable();
        uv_buff.Bind();
        uv_attr.Configure( 2, glo::DataType::FLOAT,
                           false, 0, nullptr );

        normal_attr.Enable();
        normal_buff.Bind();
        normal_attr.Configure( 3, glo::DataType::FLOAT,
                           false, 0, nullptr );

        buff.Draw();
        uv_buff.Unbind();
        attr.Disable();
        uv_attr.Disable();
        texture_buff.Unbind();
        normal_map_buff.Unbind();

        glUseProgram(0);
      });

    
    return app.Start( 60 );

  }catch( std::runtime_error err ){
    std::cerr << "Failed to initialise core of program\n"
              << "FATAL ERROR: " << err.what() << std::endl;
    getchar();
    return 1;
  }
}
