#include "attribute.hxx"
#include <GL/glew.h>

/*
  TODO: Allow attributes to own reference to a buffer they will send
  The buffer will hold a state that can be used for implicit
  operations.
*/

using namespace glo;
 
Attribute::Attribute():
  location_id( 0 )
{

}


void Attribute::Enable() const
{
  glEnableVertexAttribArray( location_id );
}


Attribute::Attribute( Attribute const& other )
{
  name = other.name;
  location_id = other.location_id;
}

void Attribute::Disable() const
{
  glDisableVertexAttribArray( location_id );
}

Attribute::~Attribute()
{
  Disable();
}


void Attribute::Configure( GLuint size, DataType type, bool norm,
                           GLuint stride, void* offset )
{
  glVertexAttribPointer( location_id, size, static_cast<GLuint>(type),
                         norm ? GL_TRUE : GL_FALSE, stride, (void*) offset );
}


GLuint Attribute::GetLocationID() const
{
  return location_id;
}

Uniform::Uniform( std::string const& nm, GLuint id ):
  name( nm ), uniform_id( id )
{
  
}


Uniform::Uniform( Uniform const& other ):
  Uniform(other.name, other.GetUniformID() )
{
}

GLuint Uniform::GetUniformID() const
{
  return uniform_id;
}

void Uniform::Set( Mat4x4 mat, bool transpose  )
{
  glUniformMatrix4fv( uniform_id, 1, transpose ? GL_TRUE : GL_FALSE,
                      &mat[0][0] );
}

void Uniform::Set( Mat3x3 mat, bool transpose  )
{
  glUniformMatrix3fv( uniform_id, 1, transpose ? GL_TRUE : GL_FALSE,
                      &mat[0][0] );
}

void Uniform::Set( Vec2f vec )
{
  glUniform2fv( uniform_id, 1, &vec[0] );
}

void Uniform::Set( Vec3f vec )
{
  glUniform3fv( uniform_id, 1, &vec[0] );
}

void Uniform::Set( int val )
{
  glUniform1i( uniform_id, val );
}
