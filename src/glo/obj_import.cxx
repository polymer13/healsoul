#include "obj_import.hxx"
#include <fstream>
#include <sstream>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_string.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

using namespace glo;

struct ObjIndex
{
  uint32_t v;
  uint32_t vt;
  uint32_t vn;
  char polygon_size;
};

void glo::load_obj_file( std::string const& path, ObjData& data )
{
  namespace qi = boost::spirit::qi;
  namespace ascii = boost::spirit::ascii;

  using qi::float_;
  using qi::uint_;
  using qi::string;
  using qi::char_;
  using qi::_1;
  using qi::phrase_parse;
  using ascii::space;
  using boost::phoenix::ref;

  std::stringstream builder;
  std::fstream in_stream( path, std::ios::in );
  std::vector<Vec3f> tmp_verticies;
  std::vector<Vec2f> tmp_uvs;
  std::vector<Vec3f> tmp_normals;
  std::vector<ObjIndex> tmp_indecies;

  if( ! in_stream.good() ){
    builder << "Could not open OBJ file: " << path;
    throw std::runtime_error( builder.str() );
    builder.clear();
  }

  tmp_verticies.clear();
  tmp_uvs.clear();
  tmp_normals.clear();

  std::string current_mesh;

  size_t line_number = 0;
  Vec3f tmp_vec( 0,0,0 );
  Vec2f tmp_uv( 0, 0 );
  Vec3f tmp_normal( 0,0,0 );
  ObjIndex tmp_index;
  tmp_index.v = 0;
  tmp_index.vt = 0;
  tmp_index.vn = 0;

  char polygon_size = 0;
  auto index_found_v_only = [&]( uint32_t v_val ){
    tmp_index.v = v_val;
    tmp_index.vt = 0;
    tmp_index.vn = 0;
    tmp_indecies.push_back( tmp_index );
  };


  auto index_found_vt = [&]( boost::optional<uint32_t> vt_val ){
    if( vt_val )
      tmp_index.vt = *vt_val;
    else
      vt_val = 0;
  };

  auto index_found_vn = [&]( uint32_t vn_val ){
    tmp_index.vn = vn_val;
    tmp_indecies.push_back( tmp_index );
  };


  std::string line;
  while( ! in_stream.eof() ){ 
    polygon_size = 0;
    line_number++;
    std::getline( in_stream, line );
    bool r = false;
    // Commented line
    if( line[0] == '#' )
      continue;

    // Vertex Definition
    r = phrase_parse( line.begin(), line.end(),
                      (
                       char_('v') >> float_[ref(tmp_vec.x) = _1]
                       >> float_[ref(tmp_vec.y) = _1] >> float_[ref(tmp_vec.z) = _1]
                       ),
                      ascii::space
                      );
    if( r ){
      tmp_verticies.push_back( tmp_vec );
      continue;
    }
    r = false;

    r = phrase_parse( line.begin(), line.end(),
                      (
                       string("vt") >> float_[ref(tmp_uv.x) = _1]
                       >> float_[ref(tmp_uv.y) = _1]
                       ),
                      ascii::space
                      );

    if( r ){ 
      tmp_uvs.push_back( tmp_uv );
      continue;
    }
    r = false;

    r = phrase_parse( line.begin(), line.end(),
                      (
                       string("vn") >> float_[ref(tmp_normal.x) = _1]
                       >> float_[ref(tmp_normal.y) = _1]
                       >> float_[ref(tmp_normal.z) = _1]
                       ),
                      ascii::space
                      );
    if( r ){
      tmp_normals.push_back( tmp_normal );
      continue;
    }
    r = false;

    r = phrase_parse( line.begin(), line.end(),
                      char_('f') >>
                      (
                       *(uint_[ref(tmp_index.v ) = _1 ] >> '/' >>
                         -(uint_[index_found_vt]) >> '/' >>
                         uint_[index_found_vn])  
                       ),
                        ascii::space  );

    if( r ){
      continue;
    }

    r = phrase_parse( line.begin(), line.end(),
                      char_('f') >>
                      *uint_[index_found_v_only] ,
                      ascii::space  );
  }

  // Now resolve indecies 

  for( auto index : tmp_indecies ){
    if( index.v != 0 ){
      data.verticies.push_back( tmp_verticies[index.v-1].x );
      data.verticies.push_back( tmp_verticies[index.v-1].y );
      data.verticies.push_back( tmp_verticies[index.v-1].z );
    }
    if( index.vt != 0 ){
      data.uvs.push_back( tmp_uvs[index.vt-1].x);
      data.uvs.push_back( 1.0 - tmp_uvs[index.vt-1].y);
    }
    if( index.vn != 0 ){
      data.normals.push_back( tmp_normals[index.vn-1].x);
      data.normals.push_back( tmp_normals[index.vn-1].y);
      data.normals.push_back( tmp_normals[index.vn-1].z);
    }
  }

}
