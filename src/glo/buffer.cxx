#include "buffer.hxx"
#include <iostream>
#include <cstring>


using namespace glo;

Buffer::Buffer( Buffer::Type t, Buffer::Mode m  ):
  buffer_id( 0 ), type( t ), mode( m ) 
{
  switch( t ) {
  case Type::ARRAY_BUFFER:
    glGenBuffers( 1, &buffer_id );
    break;
  case Type::TEXTURE_BUFFER:
    glGenTextures( 1, &buffer_id );
    break;
  }
}

void Buffer::SetType( Buffer::Type new_type )
{
  type = new_type;
}


GLuint Buffer::GetID() const
{
  return buffer_id;
}

void Buffer::InitID()
{
  glGenBuffers(1, &buffer_id );
}

void Buffer::Bind() const
{
  glBindBuffer( static_cast<GLuint>(type), buffer_id );
}

void Buffer::Unbind() const
{
  glBindBuffer( static_cast<GLuint>(type), 0 );
}


void Buffer::Draw() const
{
  glDrawArrays( GL_TRIANGLES, 0, buff.size() );
}

void Buffer::Refresh()
{
  Bind();
  std::cout << "Buffer size: " << buff.size() << std::endl;
  glBufferData( static_cast<GLuint>(type), buff.size(), &buff[0],
                static_cast<GLuint>(mode) );
  Unbind();
}

void Buffer::Set( size_t idx, float val )
{
  char tmp[sizeof(float)];
  memcpy( tmp, &val, sizeof(float) );

  for( size_t i = 0; i < sizeof(float); i++ ){
    buff.insert( buff.begin()+i+(idx*sizeof(float)), tmp[i])  ;
  }
}


void Buffer::Set( size_t idx, int val )
{
  char* tmp = (char*) &val;
  for( size_t i = 0; i < sizeof(int); i++ ){
    buff.insert( buff.begin()+idx*sizeof(int), tmp[i] );
  }
}

void Buffer::Set( size_t idx, heal::math::Vec2f val )
{
  Set( idx, val.x);
  Set( idx+1, val.y );
}

void Buffer::Set( size_t idx, heal::math::Vec3f val )
{
  Set( idx, val.x);
  Set( idx+1, val.y );
  Set( idx+2, val.z );
}


void Buffer::CopyFrom( size_t offset, std::vector<float>& list )
{
  for( size_t x = 0; x < list.size(); x++ ){
    Set( offset+x, list[x] );
  }
}


void Buffer::CopyFrom( size_t offset, size_t count, const float* arr )
{
  for( int i = 0; i < count; i++ )
    Set( offset+i, arr[i] );
}


void Buffer::CopyFrom( size_t offset, std::vector<int>& list )
{
  for( size_t x = 0; x < list.size(); x++ ){
    Set( offset+x, list[x] );
  }
}

void Buffer::CopyFrom( size_t offset, std::vector<heal::math::Vec2f>& list )
{
  for( size_t x = 0; x < list.size(); x++ ){
    Set( offset+x, list[x] );
  }
}

void Buffer::CopyFrom( size_t offset, std::vector<heal::math::Vec3f>& list )
{
  for( size_t x = 0; x < list.size(); x++ ){
    Set( offset+x, list[x] );
  }
}

Buffer::Type Buffer::GetType() const
{
  return type;
}


Buffer::Mode Buffer::GetMode() const
{
  return mode;
}


size_t Buffer::GetSize( glo::DataType t ) const
{
  using glo::DataType;

  switch( t ){
  case DataType::FLOAT:
    return buff.size() / sizeof(float);
  case DataType::INT:
    return buff.size() / sizeof(int);
  default:
    return buff.size();
  }
}

void Buffer::CopyFrom( size_t offset, size_t count, const uint8_t* data )
{
  for( size_t i = 0; i < count; i++ ){
    buff.insert( buff.begin()+offset+i, data[i] );
  }
}


void Sampler2D::SetWidth( size_t new_width )
{
  width = new_width;
}

void Sampler2D::SetHeight( size_t new_height )
{
  height = new_height;
}

glo::Sampler2D::Sampler2D( size_t w, size_t h ):
  width( w ), height( h ), Buffer( Buffer::Type::TEXTURE_BUFFER,
                                   Buffer::Mode::STATIC_DRAW )
{
}

void Sampler2D::Bind() const
{
  glBindTexture( GL_TEXTURE_2D, buffer_id );
}

void Sampler2D::Draw() const
{

}

void Sampler2D::Refresh()
{
  Bind();
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, &buff[0] );


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
	glGenerateMipmap(GL_TEXTURE_2D);

}

void Sampler2D::Unbind() const
{
  glBindTexture( GL_TEXTURE_2D, 0 );
}

void Sampler2D::InitID() 
{
  glGenTextures(1, &buffer_id);
}
