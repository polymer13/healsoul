#ifndef HEALSOUL_GLO_TYPES_HEADER_HXX
#define HEALSOUL_GLO_TYPES_HEADER_HXX
#include <memory>

namespace glo
{
  class Buffer;
  class Attribute;
  class Uniform;
  class ShaderProgram;

  typedef std::shared_ptr<Buffer> BufferPtr;
  typedef std::shared_ptr<Attribute> AttributePtr;
  typedef std::shared_ptr<Uniform> UniformPtr;
  typedef std::shared_ptr<ShaderProgram> ShaderProgramPtr;
}

#endif 
