#ifndef HEALSOUL_GLO_ATTRIBUTE_HEADER_HXX
#define HEALSOUL_GLO_ATTRIBUTE_HEADER_HXX

#include "gl_includes.hxx" 
#include <string>
#include "types.hxx"
#include "mathem.hxx"
// #include "shader.hxx"

namespace glo
{

  class ShaderProgram;
  class Attribute
  {
    friend class ShaderProgram;
    std::string name;
    GLuint location_id;

    Attribute();
  public:
    void Enable() const;
    void Disable() const;
    void Configure( GLuint size, DataType type, bool norm,
                                 GLuint stride, void* offset );


    GLuint GetLocation() const;

    Attribute( Attribute const& other );
    GLuint GetLocationID() const;
    ~Attribute();
  };

  
  class Uniform
  {
  protected:
    friend class ShaderProgram;
    std::string name;
    GLuint uniform_id;

    void InitID();

    Uniform( std::string const& nm, GLuint i );
  public:

    void Set( Mat4x4 mat, bool transpose );
    void Set( Mat3x3 mat, bool transpose );
    void Set( Vec2f vec );
    void Set( Vec3f vec );
    void Set( int val );

    void Refresh() const;

    Uniform( Uniform const& u );
    GLuint GetUniformID() const;
  };



}
#endif // HEALSOUL_GLO_ATTRIBUTE_HEADER_HXX
