#ifndef HEALSOUL_GLO_MATHEM_HEADER_HXX
#define HEALSOUL_GLO_MATHEM_HEADER_HXX
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"


namespace glo
{


  using namespace glm;

  typedef glm::mat4 Mat4x4;
  typedef glm::mat3 Mat3x3;
  typedef glm::vec3 Vec3f;
  typedef glm::vec2 Vec2f;
}

#endif
