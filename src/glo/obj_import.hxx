#ifndef HEALSOUL_GLO_OBJ_IMPORT_HEADER_HXX
#define HEALSOUL_GLO_OBJ_IMPORT_HEADER_HXX

#include <vector>
#include <string>

#include "mathem.hxx"

namespace glo
{
  
  struct ObjData 
  {
    std::vector<float> verticies;
    std::vector<float> uvs;
    std::vector<float> normals;
  };

  void load_obj_file( std::string const& path, ObjData& data );

}


#endif
