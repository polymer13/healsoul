#ifndef HEALSOUL_GLO_GL_INCLUDES_HEADER_HXX
#define HEALSOUL_GLO_GL_INCLUDES_HEADER_HXX

#include <GL/glew.h>
#include <SFML/OpenGL.hpp>

namespace glo
{

  enum class DataType{
    FLOAT = GL_FLOAT,
    INT= GL_INT,
    BYTE = GL_BYTE,
    USHORT = GL_UNSIGNED_SHORT,
    UINT = GL_UNSIGNED_INT,
  };

}

#endif
