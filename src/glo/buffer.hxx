#ifndef HEALSOUL_GLO_BUFFER_HEADER_HXX
#define HEALSOUL_GLO_BUFFER_HEADER_HXX
#include "gl_includes.hxx"
#include <healsoul/core/mathem.hxx>
#include <vector>

namespace glo
{
  

  class Buffer
  {
  public:
    enum class Type{
      ARRAY_BUFFER = GL_ARRAY_BUFFER,
      TEXTURE_BUFFER = GL_TEXTURE_BUFFER,
      UNIFORM_BUFFER = GL_UNIFORM_BUFFER
    };

    enum class Mode{
      STATIC_DRAW = GL_STATIC_DRAW,
      STATIC_READ = GL_STATIC_READ,
      // STATIC_WRITE = GL_STATIC_WRITE,
      DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
      DYNAMIC_READ = GL_DYNAMIC_READ,
      // DYNAMIC_WRITE = GL_DYNAMIC_WRITE,
      STREAM_DRAW = GL_STREAM_DRAW,
      STREAM_READ = GL_STREAM_READ,
      // STREAM_WRITE = GL_STREAM_WRITE,
    };

    virtual void InitID();
  protected:
    void SetType( Buffer::Type new_type );

    GLuint buffer_id;
    Type type;
    Mode mode;
    std::vector<char> buff;
  public: 

    explicit Buffer( Buffer::Type t, Buffer::Mode m ); 

    virtual void Draw() const;
    virtual void Refresh();
    virtual void Bind() const;
    virtual void Unbind() const;

    void Set( size_t idx, float val );
    void Set( size_t idx, int val );
    void Set( size_t idx, heal::math::Vec2f val );
    void Set( size_t idx, heal::math::Vec3f val );
    void CopyFrom( size_t offset, std::vector<float>& list );
    void CopyFrom( size_t offset, std::vector<int>& list );
    void CopyFrom( size_t offset, std::vector<heal::math::Vec2f>& list );
    void CopyFrom( size_t offset, std::vector<heal::math::Vec3f>& list );
    void CopyFrom ( size_t offset, size_t count, const float* arr );
    void CopyFrom( size_t offset, size_t count, const uint8_t* data );

    template<typename Iterator>
    void CopyFrom( size_t offset, Iterator begin, Iterator end )
    {
      if( begin == end )
        return;
      size_t type_size = sizeof( decltype( *begin ));
      size_t i = 0;
      while( begin != end ){
        Set( i*type_size+offset,  *begin );
        begin++;
        i++;
      }
    }


    Buffer::Type GetType() const;
    Buffer::Mode GetMode() const;


    size_t GetSize( glo::DataType t ) const;
    GLuint GetID() const;
  };


  class Sampler2D : public Buffer 
  {
    size_t width;
    size_t height;
  protected:
    virtual void InitID();
  public:
    virtual void Bind() const;
    virtual void Draw() const;
    virtual void Refresh();
    virtual void Unbind() const;

    void SetWidth( size_t new_width );
    void SetHeight( size_t new_height );

    explicit Sampler2D ( size_t width, size_t height );
    
  };


  // class StaticBuffer : public Buffer
  // {
  // };

  // class DynamicBuffer : public Buffer
  // {
  // };

  // class VertexBuffer : public StaticBuffer
  // {
  //   GLuint buffer_id;
  // public:
  //   VertexBuffer( );
  // };

  // class DynVertexBuffer : public DynamicBuffer
  // {

  // };

}

#endif
