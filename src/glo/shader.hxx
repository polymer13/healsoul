#ifndef HEALSOUL_GLO_SHADER_HEADER_HXX
#define HEALSOUL_GLO_SHADER_HEADER_HXX

#include "gl_includes.hxx"
#include "types.hxx"

#include <string>

namespace glo
{

  class ShaderProgram
  {
    GLuint program_id;
  public:
    ShaderProgram( std::string vertex_file_path,
                   std::string fragment_file_path );

    ShaderProgram( ShaderProgram const& other );

    GLuint GetProgramID() const;
    Attribute GetAttribute( std::string const& name );
    glo::Uniform GetUniform( std::string const& name );
    void Use() const;
  };

}
#endif 
