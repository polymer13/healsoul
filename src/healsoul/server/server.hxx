#ifndef MOD_SERVER_HEADER_HXX
#define MOD_SERVER_HEADER_HXX
#include <string>

namespace back
{

  class Server
  {
  public:
    std::string name;
    Server( std::string srv_name );
  };

}

#endif
