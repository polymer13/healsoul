#include "healsoul/core/curve.hxx"
#include "core.hxx"
#include <iostream>

using namespace heal;
using namespace heal::geom;
using namespace heal::math;

Vec2f Curve::Along( float t_value ) const
{
  if( t_value > 1 || t_value < 0 ) {
    std::cerr << "Warning: t_value exeded 1!\n";
    t_value = 0;
  }
 
  float t_rem = 1 - t_value;
  Vec2f ab (point_b - point_a);
  Vec2f bc ( point_b - point_c);
  Vec2f point_q ( point_a.x + ab.x * t_value, point_a.y + ab.y * t_value ); 
  Vec2f point_r ( point_c.x + bc.x * t_rem, point_c.y + bc.y * t_rem );
  Vec2f qr ( point_r - point_q);
  Vec2f point_p ( qr.x*t_value, qr.y*t_value);

  return point_q + point_p;
}

void Curve::SetPointA( Vec2f a )
{
  point_a = a;
}

void Curve::SetPointB( Vec2f b )
{
  point_b = b;
}

void Curve::SetPointC( Vec2f c )
{
  point_c = c;
}

Curve::Curve( Vec2f a, Vec2f b, Vec2f c ):
  point_a ( a ), point_b( b ), point_c( c )
{
}

CurveIter Curve::GetIter() const
{
  CurveIter( (*this), 0.01 );
}

void CurveIter::Restart()
{
  t_value = 0;
}

const Vec2f& CurveIter::operator* ()
{
  tmp = my_path->Along( t_value );
  return tmp;
}


CurveIter& CurveIter::operator++(int)
{
  t_value += accumulator;
  if( t_value > 1 ){
    t_value = 1;
    accumulator = -accumulator;
  }

  if( t_value < 0 ){
    t_value = 0;
    accumulator = -accumulator;
  }
  return (*this);
}

CurveIter::CurveIter( const Curve& my_path, float acc ):
  my_path( &my_path ), accumulator( acc ), t_value( 0 )
{
  
}


float CurveIter::GetT() const
{
  return t_value;
}
