#ifndef HEALSOULS_CORE_TYPES_HEADER_HXX
#define HEALSOULS_CORE_TYPES_HEADER_HXX

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>
#include <memory>
#include <functional>

namespace heal
{

namespace gui
{
  class Widget;
  class BoxWidget;
  class Button;
  class Label;
  class InputField;
}

namespace core
{
  class App;
  typedef sf::Color Color;
  typedef sf::RenderWindow Window;
  typedef std::shared_ptr<Window> WindowPtr;
  typedef sf::RenderTarget RenderTarget;
  typedef sf::RenderTexture RenderTexture;
  typedef std::shared_ptr<sf::RenderTarget> RenderTargetPtr;
  typedef std::shared_ptr<gui::Widget> WidgetPtr;
  typedef std::shared_ptr<gui::BoxWidget> BoxWidgetPtr;
  typedef std::vector<WidgetPtr> WidgetGroup; 
  typedef std::vector<WidgetPtr>::iterator  WidgetGroupIterator; 
  typedef sf::Clock Clock;
  typedef std::shared_ptr<gui::Button> ButtonPtr;
  typedef std::shared_ptr<gui::Label> LabelPtr;
  typedef std::shared_ptr<gui::InputField> InputFieldPtr;
  typedef sf::Event Event;
  using KeyboardHandler = std::function<bool(Event const&)>;
  using Keyboard = sf::Keyboard;
}

}

#endif 
