#include "mathem.hxx"
#include <cmath>
#include <iostream>

using namespace heal;
using namespace heal::math;
using Vec2f = sf::Vector2f;
using Vec2i = sf::Vector2i;

float math::magnitude ( Vec2f v )
{
  float x = v.x * v.x;
  float y = v.y * v.y;
  return std::sqrt( x + y );
}

float math::angle_from_origin( Vec2f f )
{
  return (180 / M_PI) * std::atan( f.y / f.x );
}

math::Vec2f operator*( math::Vec2f const& a, float s )
{
  return math::Vec2f( a.x * s, a.y * s );
}
math::Vec2f operator*( float s, math::Vec2f const& a )
{
  return math::Vec2f( a.x * s, a.y * s );
}

float math::dot( const math::Vec2f& a, const math::Vec2f& b )
{
  return a.x * b.x + a.y * a.y;
}

float math::self_dot( const math::Vec2f& a )
{
  return a.x * a.x + a.y * a.y;
}


Transform2D&  Transform2D::Translate( Vec2f by )
{
  trans.translate( by );
  return (*this);
}

Transform2D&  Transform2D::Translate( float x, float y )
{
  trans.translate( x, y );
  return (*this);
}

Transform2D&  Transform2D::Rotate( float by, float center_x, float center_y )
{
  trans.rotate( by, center_x, center_y );
  return (*this);
}

Transform2D&  Transform2D::Rotate( float by, Vec2f center )
{
  trans.rotate( by, center );
  return (*this);
}
 

Transform2D&  Transform2D::Rotate( float by )
{
  trans.rotate( by );
  return (*this);
}
 
Transform2D&  Transform2D::Scale( Vec2f by )
{
  trans.scale( by );
  return (*this);
}

Transform2D Transform2D::GetInverse()
{
  return Transform2D ( trans.getInverse() );
}


Transform2D::Transform2D( sf::Transform t ):
  trans( t )
{

}


Transform2D::Transform2D():
  trans()
{
  
}

std::ostream& operator<<( std::ostream& out, math::Vec2f const& a )
{
  out << "{ x: " << a.x <<", y: " << a.y << " }";
  return out;
}
