#ifndef HEALSOUL_CORE_SPRITE_HEADER_HXX
#define HEALSOUL_CORE_SPRITE_HEADER_HXX
#include "shape.hxx"

namespace heal
{
  namespace  gfx
  {
    class Sprite : public heal::gfx::Rect 
    {
      Sprite( std::string const& path );
      void SetImage( std::string const& path );
    };
  }
}

#endif // HEALSOUL_CORE_SPRITE_HEADER_HXX
