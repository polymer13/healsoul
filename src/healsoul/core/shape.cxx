#include "healsoul/core/shape.hxx"
#include <iostream>
//#include "healsoul/core/mathem.hxx"

using namespace heal;
using namespace heal::core;
using namespace heal::gfx;


// TODO: Rotation should be around center of a rectangle
// Angle thus "Contains" needs to take that into account 

// TODO: Possibly unify corner methods 

// TODO: Implement "Contains" for all shapes (Line, Rect ++)

bool gfx::Rect::Contains( Vec2f const& point  ) const
{
  // Rotate both me to 0 angle and rotate "point"
  // equally also 

}


Vec2f gfx::Rect::Center() const
{
  return position + (0.5 * size);
}


math::Vec2f gfx::Shape::GetXY() const
{
  return position;
}


math::Vec2f gfx::Rect::GetSize() const
{
  return size;
}

Vec2f gfx::Rect::TopRight() const
{
  math::Transform2D trans;
  trans.Rotate( rotation, this->Center() );
  trans.Translate( position );
  
  Vec2f tmp( position.x, position.y + size.y );
  return tmp;
}

Vec2f gfx::Rect::TopLeft() const
{
  return position;
}

Vec2f gfx::Rect::BottomLeft() const
{
  Vec2f tmp( position.x, position.y + size.y );
  
  return tmp;
}

Vec2f gfx::Rect::BottomRight() const
{
  return position + size;
}


float gfx::Rect::Top() const
{
  return position.y;
}

float gfx::Rect::Bottom() const
{
  return size.y + position.y;
}

float gfx::Rect::Left() const
{
  return position.x;
}

float gfx::Rect::Right() const
{
  return size.x + position.x;
}

void Line::Refresh()
{
  Vec2f tmp = destination - position;
  float len = math::magnitude( tmp );
  my_rect.setPosition( position );
  my_rect.setSize( Vec2f( len, thickness ) );
  float angle =  math::angle_from_origin( tmp );
  my_rect.setRotation( angle );
  my_rect.setFillColor( color );
}

void Line::Draw( core::RenderTargetPtr window ) const
{
  window->draw( my_rect );
}


void Shape::SetXY( Vec2f  p )
{
  position = p;
}

void Line::SetFromTo( Vec2f f, Vec2f t )
{
  position = f;
  destination = t;
}

Line::Line(  Vec2f from , Vec2f to, core::Color col ):
  destination( to ), color( col ), thickness( 1 )
{
  position = from;
}


void Line::SetLength( float l )
{
  Vec2f dir =  destination - position;
  dir.x *= l;
  dir.y *= l;
  destination = position + dir;
}

// void Line::SetFromTo( const Vec2f& f, const Vec2f& t )
// {
//   position = f;
//   destination = t;
// }

void Line::SetThickness( float t )
{
  thickness = t;
}

void Line::SetColor( core::Color col )
{
  color = col;
}


void Line::SetLikeRay( Vec2f origin, Vec2f dir,  float len )
{
  position = origin;
  destination = origin + (dir * len);
}

gfx::Rect::Rect( math::Vec2f pos , math::Vec2f size ):
  size( size )
{
  position = std::move( pos );
}


void Rect::SetRotation( float angle )
{
  rotation = angle;
}

void ColorRect::Refresh()
{
  my_rect.setPosition( GetXY() );
  my_rect.setSize( GetSize() );
  // float angle =  math::angle_from_origin( tmp );
  my_rect.setRotation( rotation );
  my_rect.setFillColor( back_color );
}

void ColorRect::Draw( core::RenderTargetPtr window ) const
{
  window->draw( my_rect );
}

void Rect::SetSize( float w, float h )
{
  size = std::move( math::Vec2f( w, h ) );
}

void Rect::SetSize( Vec2f const& sz )
{
  size = sz;
}

void Rect::SetSize( Vec2f&& sz )
{
  size = std::move ( sz );
}

void ColorRect::SetFaceColor( core::Color c )
{
  face_color = c;
}

void ColorRect::SetBackColor( core::Color c )
{
  back_color = c;
}

ColorRect::ColorRect( Vec2f pos , Vec2f sz, core::Color bc , core::Color fc  ):
  back_color( bc ), face_color( fc )
{
  SetXY( std::move( pos ) );
  SetSize( std::move( sz ) );
}

bool gfx::Line::Contains( math::Vec2f const& v ) const
{
  
}
