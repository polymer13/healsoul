#include "grid.hxx"
#include "shape.hxx"
#include "types.hxx"

using namespace heal;
using namespace heal::gfx;

Grid::Grid(   Vec2f pos, Vec2f sz , Color cl, int ln_off ):
  line_offset( ln_off ), color ( cl )
{
  size = std::move( sz );
  position = std::move( pos );
}

void Grid::SetOffset( int new_offset )
{
  line_offset = new_offset;
}

void Grid::Refresh()
{
  
}

void Grid::Draw( RenderTargetPtr window ) const
{
  Vec2f from = position;
  Vec2f to = position;
  to.x += size.x;
  gfx::Line line ( from, to, color );

  //Vertical lines
  for( int y = 0; y <= size.y; y += line_offset  ){
    line.SetFromTo( from, to );
    line.Refresh();
    line.Draw( window );
    from.y += line_offset;
    to.y += line_offset;
  }

  from = position;
  to = position;
  to.y += size.y;

  // Horizontal lines
  for( int x = 0; x <= size.x; x += line_offset ){
    line.SetFromTo( from, to );
    line.Refresh();
    line.Draw( window );
    from.x += line_offset;
    to.x += line_offset;
  }
}
