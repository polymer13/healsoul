#ifndef HEALSOUL_CORE_GRID_HEADER_HXX
#define HEALSOUL_CORE_GRID_HEADER_HXX

#include "core.hxx"

namespace heal
{

namespace gfx
{
  using namespace core;
  using namespace gui;
  using Color = sf::Color;

  // Setters for size should be in Rect class 

  class Grid : public Rect 
  {
    int line_offset;
    Color color;
  public:
    Grid( Vec2f pos = Vec2f(0,0),
          Vec2f sz = Vec2f(10,10), Color cl = Color::Green,
          int line_offset = 10 );
    void SetOffset( int p ); 
    virtual void Draw( RenderTargetPtr win ) const;
    virtual void Refresh();
  };
}


}
#endif 
