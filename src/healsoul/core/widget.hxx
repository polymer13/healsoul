#ifndef HEALSOUL_CORE_WIDGET___HEADER_HXX
#define HEALSOUL_CORE_WIDGET___HEADER_HXX
#include <functional>
#include <memory>

#include "core.hxx"
#include "shape.hxx"
#include "mathem.hxx"
#include "types.hxx"

namespace heal{

  namespace core
  {
    extern sf::String default_font_path; 
    extern sf::Font default_font;
    extern int default_font_size;
    extern core::WindowPtr default_window;
  }

  namespace gui
  {
    using namespace core;
    using math::Vec2f;
    using Color = sf::Color;


    struct ClickInfo
    {
      enum ClickType{
                     CURSOR_CLICK,SUBMIT_CLICK
      } click_method;
      heal::math::Vec2f position;
      float time;
      ClickInfo( ClickType cm, heal::math::Vec2f pos, float t ):
        click_method(cm), position(pos), time(t)
      {}

    };

    class Widget : gfx::Rect
    {
    protected:
      friend class core::App;

      WidgetPtr my_parent;
      sf::Rect<float> bounds;
      bool was_hovered;
      bool changed;
      bool is_active;
      std::string name;
    protected:
      Color color;
      Color back_color; 
      bool selectable;
      bool keyboard_ready;
      void SetSelectable( bool yesno );
      void SetKeyboardReady( bool yesno ); 
    public:
      virtual bool KeyboardInput( heal::core::Event const& ev );
      virtual void Hovered( bool entered );
      virtual void Clicked( ClickInfo c );
      virtual void Draw( RenderTargetPtr win ) const = 0;
      virtual bool Contains( Vec2f const& pos ) const = 0;
      virtual void Refresh() = 0;

      void SetSize( float w, float h );
      void SetSize( Vec2f s );
      void SetXY( Vec2f pos );
      void SetXY( float x, float y );
      Vec2f GetXY() const;
      Vec2f GetSize () const;

      float Top() const;
      float Bottom() const;
      float Right() const;
      float Left() const;

      void SetName( std::string name );
      bool IsActive() const;
      bool IsSelectable() const;
      bool IsKeyboardReady() const;
      void SetActive( bool yes );
      void SetFaceColor( Color col );
      void SetBackColor( Color col );
      void SetParent( WidgetPtr new_parent );
      WidgetPtr GetParent();

      // void AddClickEvent( std::function<void()> cb );
      // void AddHoverEvent( std::function<void(bool)> cb );
    };

    class BoxWidget : public Widget
    {
    protected:
      friend class core::App;
      sf::String text;
      float outline_thickness;
      sf::Text my_text;
      sf::RectangleShape my_rect;
      Color text_color;
      Color outline_color;
    public:
      virtual void Draw( RenderTargetPtr win ) const ; 
      virtual void Refresh();
      virtual bool Contains( math::Vec2f const& p ) const ;

      BoxWidget( sf::String text, Vec2f pos = Vec2f(0,0),
                 Vec2f sz = Vec2f( 30, 15 ),
                 Color color = Color( 0x000000dd ),
                 Color text_color = Color::Green,
                 Color outline_color = Color::Green,
                 float outline_thickness = 5 );
      void SetText ( sf::String );
    };


    class Button : public BoxWidget
    {
    private:
      using ClickAction = std::function<void(ClickInfo)>;
      bool is_hovered; 
      Color hover_color;
      ClickAction click_action;
    public:
      virtual void Refresh();
      explicit Button( sf::String text,  
                       Vec2f pos = Vec2f(0,0),
                       Vec2f sz = Vec2f( 30, 15 ),
                       Color color = Color( 0x000000dd ),
                       Color hover = Color( 0x113311ee ));
      void Hovered( bool ) override;
      void SetClickAction( ClickAction c );
      void Clicked( ClickInfo c ) override;
    };

    class InputField : public BoxWidget
    {
    private:
      std::function<void(sf::String)> text_receiver;
    public:
      explicit InputField( sf::String text, 
                           Vec2f pos = Vec2f ( 0, 0 ),
                           Vec2f sz = Vec2f( 40, 15 ),
                           Color color = Color( 0x000000dd ) );
      void AddChar ( char c );
      void RemoveLastChar();
      void SetTextReciever( std::function<void(sf::String)> );
      virtual void Refresh() override;
      bool KeyboardInput( heal::core::Event const& ev ) override;
    };

    class Label : public BoxWidget
    {
    public:
      explicit Label( sf::String text, 
                      Vec2f pos = Vec2f ( 0, 0 ),
                      Vec2f sz = Vec2f( 40, 15 ),
                      Color color = Color::Green );
      void Draw( RenderTargetPtr win ) const override;
    };

  }

}
#endif //_HEADER_WIDGET_HXX_
