#include "image.hxx"

using namespace heal::gfx;

heal::gfx::Image::Image()
{
  
}


bool Image::LoadFromFile( std::string const& path )
{
  return image.loadFromFile( path );
}


const uint8_t* Image::GetPixelsPtr() const
{
  return image.getPixelsPtr();
}

size_t Image::GetPixelCount() const
{
  auto a = image.getSize();
  return a.x * a.y;
}

size_t Image::GetByteCount() const
{
  auto a = image.getSize();
  return a.x * a.y * 4;
}

heal::math::Vec2u Image::GetSize() const
{
  return image.getSize();
}
