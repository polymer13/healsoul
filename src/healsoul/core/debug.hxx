#ifndef HEALSOUL_CORE_DEBUG_HEADER_HXX
#define HEALSOUL_CORE_DEBUG_HEADER_HXX
#include <iostream>

namespace heal
{
  

namespace debug
{

#define print_val( val ) std::cout << #val << val << std::endl;

}

}
#endif
