#ifndef _HEADER_CORE_TIMERS_HXX_
#define _HEADER_CORE_TIMERS_HXX_
#include <algorithm>

namespace heal
{

template<typename T>
class LinearStep
{
public:
  int directions;
  T current_value;
  T accumulator;
  T max;
  T min;
  explicit LinearStep( int dirs, T end1, T end2, T acc )
  {
    directions = dirs;
    current_value = end1;
    min = std::min( end1, end2 );
    max = std::max ( end1, end2 );
    accumulator = acc;
  }

  void SetValue( T val )
  {
    current_value = val > max ? max : val < min ? min : val;
  }

  T Next()
  {
    if( directions > 0 ){
      current_value += accumulator;
      if( current_value > max )
        current_value -= (max - min);
      return current_value;
    }else if ( directions < 0 ) {
      current_value -= accumulator;
      if( current_value < min )
        current_value -= (max - min);
      return current_value;
    }else {
      current_value += accumulator;
      if( current_value >= max ){
        current_value = max;
        accumulator = -accumulator;
      }else if ( current_value <= min ){
        current_value = min;
        accumulator = -accumulator;
      }
    }
    return current_value;
  }
};

}

#endif
