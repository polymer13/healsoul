#ifndef HEALSOUL_CORE_LAYOUT_HEADER_HXX
#define HEALSOUL_CORE_LAYOUT_HEADER_HXX
#include "widget.hxx"
#include "shape.hxx"
#include "types.hxx"

namespace heal
{
  

namespace gui
{

  // NOTE: Should RemoveWidget throw() ? 
  class Layout : public Widget
  {
  protected:
    RenderTexture my_texture;
    WidgetGroup children;
    WidgetGroupIterator first_visible;
    WidgetGroupIterator last_visible;

    gfx::ColorRect background_rect;

    float x_scroll, y_scroll;
    void scroll_x_by();
    void scroll_y_by();
    void find_visible() const;
  public:
    // virtual void Draw( RenderTargetPtr win ) const = 0;
    // virtual void Refresh() = 0;
    // virtual bool Contains( math::Vec2f const& p ) const = 0;
    void AddWidget( WidgetPtr w );
    void RemoveWidget( WidgetPtr w );
    void RemoveWidget( size_t index );
    void SetBackColor( Color col );

    // void SetVisible( size_t index, bool yes = true );
    // void SetVisibleRange( size_t first, size_t last, bool yes = true );
    // void SetVisibleWithin( gfx::Rect const& rect,  bool yes = true );

    size_t GetChildCount();
    void  Clear();
  };

  class VLayout : public Layout
  {
    float offset;
  public:
    virtual void Draw( RenderTargetPtr win ) const ;
    virtual void Refresh();
    virtual bool Contains( math::Vec2f const& p ) const;
    void SetOffset( float off );
    VLayout( math::Vec2f pos, math::Vec2f size, float off = 0 );
  };


  class HLayout : public Layout 
  {
    float offset;
  public:
    void Draw( RenderTargetPtr win ) const;
    virtual void Refresh();
    virtual bool Contains( math::Vec2f const& p ) const;
    void SetOffset( float off );
    HLayout( math::Vec2f pos, math::Vec2f size, float off = 0 );
  };

  typedef  std::shared_ptr<Layout> LayoutPtr;

  LayoutPtr make_VLayout( Vec2f const& pos, Vec2f const& size, float off = 0 );

}

}
#endif // HEALSOUL_CORE_LAYOUT_HEADER_HXX 
