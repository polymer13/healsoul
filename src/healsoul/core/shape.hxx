#ifndef HEALSOUL_CORE_SHAPES__HEADER_HXX
#define HEALSOUL_CORE_SHAPES__HEADER_HXX

#include "mathem.hxx"
// #include "core.hxx"
#include "types.hxx"

namespace heal
{

namespace gfx
{

  using math::Vec2f;
  class Shape
  {
  protected:
    math::Vec2f position;
  public:
    virtual void Draw( core::RenderTargetPtr window ) const = 0;
    virtual void Refresh() = 0;
    virtual bool Contains( Vec2f const& point ) const = 0;
    void SetXY( math::Vec2f p );
    math::Vec2f GetXY() const;
    // May implement swap ideom
    // void SetXY( const math::Vec2f&&  p );
  };

  class Rect  : public Shape
  {
  protected:
    math::Vec2f size;
    float rotation;
  public:
    // virtual void Draw( core::RenderTargetPtr window ) const = 0;
    // virtual void Refresh()  = 0 ;
    virtual bool Contains( Vec2f const& point ) const ;
    void SetSize( float width, float height );
    void SetSize( math::Vec2f const& sz  );
    void SetSize( math::Vec2f&& sz );
    Vec2f GetSize() const;
    void SetRotation( float angle );

    // Rotation is ignored by Top, Bottom, Left, Right
    float Top() const;
    float Bottom() const;
    float Left() const;
    float Right () const;
    Vec2f Center() const;

    // Corners do not ignore rotation 
    Vec2f TopRight() const;
    Vec2f TopLeft() const;
    Vec2f BottomLeft() const;
    Vec2f BottomRight() const;

    explicit Rect( math::Vec2f pos = math::Vec2f( 0,0 ),
                   math::Vec2f size = math::Vec2f( 10, 10));
  };

  class ColorRect: public Rect
  {
  protected:
    sf::RectangleShape my_rect;
    core::Color face_color;
    core::Color back_color;
  public:
    virtual void Draw( core::RenderTargetPtr window ) const ;
    virtual void Refresh();
    void SetFaceColor( core::Color c );
    void SetBackColor( core::Color c );

    ColorRect( Vec2f pos = Vec2f( 0,0 ),
               Vec2f sz  = Vec2f( 10, 10 ),
               core::Color bc  = core::Color::White,
               core::Color fc  = core::Color::Black );

  };

  class Line : public Shape
  {
    sf::RectangleShape my_rect;
    math::Vec2f destination;
    core::Color color;
    float thickness;
  public:
    Line(  math::Vec2f from = math::Vec2f( 0, 0 ),
           math::Vec2f to   = math::Vec2f( 1, 1 ),
           core::Color col  = core::Color::White );

    virtual void Draw( core::RenderTargetPtr window ) const;
    virtual void Refresh();
    virtual bool Contains( Vec2f const& point ) const ;
    void SetLength( float l );
    // void SetFromTo( const math::Vec2f& f, const math::Vec2f& t );
    void SetFromTo( Vec2f f, Vec2f t );
    void SetLikeRay( math::Vec2f origin, math::Vec2f dir,  float len );
    void SetDirection( math::Vec2f dir );
    void SetThickness( float t );
    void SetColor( core::Color col );
  };

  class LineIterator
  {
    core::RenderTargetPtr window;
  public:
    LineIterator( core::RenderTargetPtr win );
    void Next( math::Vec2f from, Vec2f to );
  };

}

}

#endif // HEALSOUL_CORE_SHAPES_HEADER_HXX
