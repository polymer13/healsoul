#ifndef HEALSOULS_CORE_MATH_HEADER_HXX
#define HEALSOULS_CORE_MATH_HEADER_HXX
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Transform.hpp>
#include <ostream>

namespace heal
{
  
namespace math
{
  using Vec2f = sf::Vector2f;
  using Vec2i = sf::Vector2i;
  using Vec2u = sf::Vector2u;

  class Vec3f
  {
  public:
    float x, y, z;
  };

  // using Transform2D = sf::Transform;

  float magnitude ( Vec2f v );
  float angle_from_origin( Vec2f f );
  float self_dot( const math::Vec2f& a );
  float dot( const Vec2f& a, const Vec2f& b );

  // TODO: Make proper Transform class

  class Transform2D
  {
    sf::Transform trans;
    Transform2D( sf::Transform t );
  public:
    Transform2D& Translate( Vec2f by );
    Transform2D& Translate( float x, float y );
    Transform2D& Rotate( float by ); 
    Transform2D& Rotate( float by, float center_x, float center_y );
    Transform2D& Rotate( float by, Vec2f center ); 
    Transform2D& Scale( Vec2f by );
    Transform2D GetInverse();
    explicit Transform2D();
  };

}

}

heal::math::Vec2f operator*( heal::math::Vec2f const& a, float s );
heal::math::Vec2f operator*( float s, heal::math::Vec2f const& a );
std::ostream& operator<<( std::ostream& out, heal::math::Vec2f const& a );

#endif
