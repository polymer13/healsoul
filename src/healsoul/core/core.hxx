#ifndef HEALSOULS_CORE_CORE__HEADER_HXX
#define HEALSOULS_CORE_CORE__HEADER_HXX

#include "widget.hxx"
#include "updates.hxx"
#include "mathem.hxx"
#include "types.hxx"
#include <algorithm>
#include <stack>

/*

 */

/* TODO: 
 *   There should be an single input handler, he should
 *   then redirect any input to other widgets
 */
namespace heal
{

namespace gui
{
  class Widget;
}

namespace core
{
  struct AppSettings
  {
    bool single_window;
    bool opengl_ready;
    unsigned char opengl_major_version;
    unsigned char opengl_minor_version;
    unsigned int depth_bits;
    unsigned int stencil_bits;
    unsigned int antialiasing_level;
  };

  AppSettings& default_app_settings();

  class CoreUpdate;
  class App
  {
    friend class CoreUpdate;
    Clock clock;
    void LoadFonts();
    bool is_running;
    math::Vec2i resolution;
    void core_update();
    math::Vec2f mouse_pos;
    bool init_done;
    bool click_unbinds_keyboard_root;
    // KeyboardHandler root_keyboard_handler;
    // std::vector<KeyboardHandler> keyboard_handlers;
    // std::function<void()> submit_handler;

    KeyboardHandler keyboard_handler;
    WidgetPtr selected_widget;

    int clear_color;
    float current_time;
    App();

    AppSettings custom_settings;

  public:
    std::vector<WidgetPtr> widgets;
    // std::vector<std::function<void( const math::Vec2f& )>> on_mouse_clicked;
    // std::vector<std::function<void( const math::Vec2f& )>> on_mouse_down;

    static App& GetInstance ();
    void Init( sf::Vector2i res, const char* title,
               AppSettings const& setup = heal::core::default_app_settings() );
    void HandleEvents();
    WidgetPtr GetSelectedWidget() const;
    // Keyboard handlers are deprecated
    // Only if widget is selectable could it
    // get keyboard input
    // void SetRootKeyboardHandler(  KeyboardHandler h, bool click_unbind = true );
    // void PushKeyboardHandler( KeyboardHandler h );
    // void PopKeyboardHandler();
    void SetKeyboardHandler( KeyboardHandler h );

    void SetClickUnbindsRootKeyboardHandler( bool yes );
    // void SetSubmitHandler( std::function<void()>  h );
    void TriggerSubmit();
    void SetClearColor( int color );
    int Start( int fps );
    void Stop();
    math::Vec2i GetResolution() const;
    math::Vec2u GetMainWindowSize() const;
    WindowPtr sfml_main_window;
    std::vector<Update> updates;

    void AddUpdate( Update up );

    template<class WidType, typename... Args>
    std::shared_ptr<WidType> NewWidget( Args... arg )
    {
      std::shared_ptr<WidType> ptr = std::make_shared<WidType>( arg... );
      widgets.push_back( std::static_pointer_cast<gui::Widget>(ptr) );
      return ptr;
    }

    template<typename... Args>
    ButtonPtr NewButton( Args... arg )
    {
      return NewWidget<gui::Button>( arg... );
    }

    template<typename... Args>
    LabelPtr NewLabel( Args... arg )
    {
      return NewWidget<gui::Label>( arg... );
    }

    template<typename... Args>
    InputFieldPtr NewInputField( Args... arg )
    {
      return NewWidget<gui::InputField>( arg... );
    }

  };

}

}
#endif //_HEADER_CORE_HXX_
