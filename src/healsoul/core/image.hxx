#ifndef HEALSOUL_CORE_IMAGE_HEADER_HXX
#define HEALSOUL_CORE_IMAGE_HEADER_HXX
#include "types.hxx"
#include <string>
#include "mathem.hxx"

namespace heal
{

  namespace gfx
  {
    class Image
    {
      sf::Image image;
    public:
      Image();
      bool LoadFromFile( std::string const& path );
      size_t GetByteCount() const;
      size_t GetPixelCount() const;
      heal::math::Vec2u GetSize() const;
      const uint8_t* GetPixelsPtr() const;
    };
  }

}

#endif
