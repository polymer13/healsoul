#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics.hpp>
#include <algorithm>
#include <functional>
#include <exception>
#include <memory>
#include <iostream>
#include <vector>
#include <map>

#include "core.hxx"


/*  TODO:
 * Make better unbind system for keyboard handlers.
 * Create root keyboard handler that cannot be.
 * It would prevent other handlers from stealing
 * InputFields input 
*/

using namespace heal;
using namespace heal::core;

using Vec2i = sf::Vector2i;
using Vec2f = sf::Vector2f;

sf::String heal::core::default_font_path ("assets/fonts/rabbit.otf");
sf::Font heal::core::default_font;
int heal::core::default_font_size = 15;
std::shared_ptr<sf::RenderWindow> heal::core::default_window;

heal::core::AppSettings& heal::core::default_app_settings() 
{

  static heal::core::AppSettings default_settings;
  default_settings.    single_window = true;
  default_settings.    opengl_ready = false;
  default_settings.    opengl_major_version = 0;
  default_settings.    opengl_minor_version = 0;
  default_settings.    depth_bits = 1;
  default_settings.    stencil_bits = 1;
  default_settings.    antialiasing_level = 0;

  return default_settings;
}


void heal::core::App::Init( sf::Vector2i res, const char* title,
                            AppSettings const& setup  )
{
  if ( ! init_done ){
    init_done = true;
    resolution = res;

    sf::ContextSettings cox_setup;

    cox_setup.antialiasingLevel = setup.antialiasing_level;

    // If opengl ready window is needed 
    if( setup.opengl_ready ){
      cox_setup.depthBits    = setup.depth_bits;
      cox_setup.majorVersion = setup.opengl_major_version;
      cox_setup.minorVersion = setup.opengl_minor_version;
      cox_setup.stencilBits  = setup.stencil_bits;
      
    }

    // Constructing SFML window 
    sfml_main_window = std::make_shared<sf::RenderWindow>( sf::VideoMode( res.x, res.y ), title, sf::Style::Resize | sf::Style::Close,   cox_setup  );
    widgets.reserve( 20 );
    LoadFonts();
    default_window = sfml_main_window;
  }
}

heal::core::App::App():
  init_done( false ),
  clear_color( 0x0 ),
  current_time( 0 ),
  keyboard_handler( []( heal::core::Event const& ) -> bool {return false;} )
{
  
}

void App::SetKeyboardHandler( KeyboardHandler h )
{
  keyboard_handler = h;
}

WidgetPtr App::GetSelectedWidget() const
{
  return selected_widget;
}

App& core::App::GetInstance()
{
  static core::App instance;
  return instance;
}

Vec2i core::App::GetResolution() const
{
  return resolution;
}

// void App::PushKeyboardHandler( KeyboardHandler h )
// {
//   // keyboard_handlers.push_back( h );
// }

// void App::SetRootKeyboardHandler( KeyboardHandler h, bool click_unbind )
// {
//   click_unbinds_keyboard_root = click_unbind;
//   // root_keyboard_handler = h;
// }

// void App::SetClickUnbindsRootKeyboardHandler( bool yes )
// {
//   // If user clicks keyboard will lose it's input handler
//   // and if a widget is clicked it will potentially
//   // register a new keyboard handler
//   click_unbinds_keyboard_root = yes;
// }


// void App::PopKeyboardHandler()
// {
//   keyboard_handlers.pop_back();
// }

void core::App::LoadFonts()
{
  if ( ! default_font.loadFromFile( core::default_font_path ) ) {
    std::cerr << "Could not load font!";
    exit(1);
  }
  std::cout << "Loaded fonts!\n";
}

void core::App::HandleEvents ()
{
  sf::Event ev;
  while( sfml_main_window->pollEvent( ev ) ) {
    switch ( ev.type ) {
    case sf::Event::Closed:
      sfml_main_window->close();
      break;
    case sf::Event::KeyPressed:
    case sf::Event::TextEntered:
      keyboard_handler( ev );
        // for( auto h : keyboard_handlers )
        //   {
        //     // Check if the input has been handled by
        //     // current keyboard handler else continue
        //     // through the stack
        //     if( h( ev ) )
        //       break;
        //   }
      break;
    default:
      break;
    }
  }

  using kb = sf::Keyboard;
#define pressed kb::isKeyPressed 
  if ( pressed( kb::Escape )  )
    sfml_main_window->close();
#undef pressed
}


void core::App::Stop()
{
  is_running = false;
}

int core::App::Start( int fps )
{
  uint32_t elapse = 1;
  uint32_t timelimit = 1000 / fps;
  uint32_t remaining = 0;
  is_running = true;
  while( sfml_main_window->isOpen() && is_running ) {
    clock.restart();

    core_update();
    std::for_each( updates.begin(), updates.end(),
                   []( std::function<void()>& update ) {
                     update();
                   });

    elapse = clock.getElapsedTime().asMilliseconds();
    remaining = timelimit - elapse;
    
current_time += elapse;
    sf::sleep( sf::milliseconds( remaining  ) );
  }

  return 0;
}

void core::App::core_update()
{
  HandleEvents();

  sfml_main_window->pushGLStates();
  Vec2i temp = sf::Mouse::getPosition ( *sfml_main_window.get() );
  math::Vec2f mp ( temp.x, temp.y );
  mouse_pos = mp;
  static bool left_mouse_was_pressed = false;
  static bool right_mouse_was_pressed = false;
  bool left_mouse_pressed = false;
  bool left_mouse_clicked = false;
  bool right_mouse_pressed = false;
  bool right_mouse_clicked = false;

  std::shared_ptr<gui::Widget> ptr;
  left_mouse_pressed = sf::Mouse::isButtonPressed( sf::Mouse::Left );
  right_mouse_pressed = sf::Mouse::isButtonPressed( sf::Mouse::Right );

  if( left_mouse_pressed ){
    left_mouse_was_pressed = true;
    // Execute all mouse press event handlers
    // std::for_each( on_mouse_down.begin(), on_mouse_down.end(),
    //                [&]( std::function<void(const Vec2f& p)> f ) {
    //                  f( mp );
    //                });
  }else {
    if ( left_mouse_was_pressed )
      left_mouse_clicked = true;
    left_mouse_was_pressed = false;
  }

  if( right_mouse_pressed ){
    right_mouse_was_pressed = true;
    // Execute all mouse press event handlers
    // std::for_each( on_mouse_down.begin(), on_mouse_down.end(),
    //                [&]( std::function<void(const Vec2f& p)> f ) {
    //                  f( mp );
    //                });
  }else {
    if ( right_mouse_was_pressed )
      right_mouse_clicked = true;
    right_mouse_was_pressed = false;
  }

  // if( left_mouse_clicked ){
  //   // Execute all mouse click event handlers
  //   // std::for_each( on_left_mouse_clicked.begin(), on_left_mouse_clicked.end(),
  //   //                [&]( std::function<void(const Vec2f& p)> f ) {
  //   //                  f( mp );
  //   //                });
  // }



  for ( size_t i = 0; i < widgets.size(); i++ ) {
    ptr = widgets[i];
    ptr->Refresh();
    if ( ptr->IsActive() ){
      if ( ptr->Contains( mp ) ){
        ptr->Hovered( true );
        if( left_mouse_clicked ){
          selected_widget = ptr;
          // ptr->TriggerClick();
          ptr->Clicked( heal::gui::ClickInfo( heal::gui::ClickInfo::CURSOR_CLICK, mp, current_time));
          if( ptr->IsKeyboardReady() && ptr->IsSelectable() )
            {
              keyboard_handler = [=]( Event const& e ){
                                  return ptr->KeyboardInput( e );
                                 };
            }
        }
      }else
        ptr->Hovered( false );

      ptr->Refresh();
      ptr->Draw( sfml_main_window );
    }

  }

  sfml_main_window->popGLStates();

  sfml_main_window->display();
  sfml_main_window->clear( Color( clear_color ) );
}

// void core::App::SetSubmitHandler( std::function<void()> h )
// {
//   // submit_handler = h;
// }

// void core::App::TriggerSubmit()
// {
//   if( submit_handler )
//     submit_handler();
// }


void core::App::AddUpdate( Update up )
{
  updates.push_back( up );
}

void core::App::SetClearColor( int color )
{
  clear_color = color;
}

math::Vec2u core::App::GetMainWindowSize() const
{
  return sfml_main_window->getSize();
}
