#ifndef HEALSOUL_CORE_UPDATES_HEADER_HXX
#define HEALSOUL_CORE_UPDATES_HEADER_HXX
#include <functional>

namespace heal
{

namespace core
{
  using Update = std::function<void()>;
  
    class  SwapUpdate
    {
      Update update_call;
    public:
    
      SwapUpdate( Update up );
        void operator() ();
      void operator= ( Update up );
    };
}

}


#endif // _HEADER_CORE_UPDATES_HXX_
