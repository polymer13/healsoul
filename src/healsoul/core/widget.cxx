#include "widget.hxx"
#include <cmath>
#include <iostream>

using namespace heal;
using namespace heal::core;
using namespace heal::gui;


float gui::Widget::Top() const
{
  return bounds.top;
}

void gui::Widget::Hovered( bool )
{
  
}

void gui::Widget::Clicked( ClickInfo )
{
  
}

void gui::Widget::SetSelectable( bool yesno )
{
  selectable = yesno;
}

bool gui::Widget::IsSelectable() const
{
  return selectable;
}

float gui::Widget::Bottom() const
{
  return bounds.top + bounds.height; 
}

float gui::Widget::Left() const
{
  return bounds.left;
}

float gui::Widget::Right() const
{
  return bounds.left + bounds.width;
}

void gui::Widget::SetParent( WidgetPtr new_parent )
{
  my_parent = new_parent;
}

WidgetPtr gui::Widget::GetParent()
{
  return my_parent;
}

void gui::Widget::SetName( std::string new_name )
{
  name = std::move( new_name );
}

void gui::Widget::SetSize ( float w, float h )
{
  changed = true;
  bounds.width = w; 
  bounds.height = h;
}

void Widget::SetSize ( Vec2f ns  )
{
  changed = true;
  bounds.width = ns.x; 
  bounds.height = ns.y; 
}

void Widget::SetFaceColor( Color cl )
{
  color = cl;
}

void Widget::SetBackColor( Color cl )
{
  back_color = cl;
}

void Widget::SetActive( bool yes )
{
  is_active = yes;
}

bool  Widget::IsActive() const
{
  return is_active;
}

void Widget::SetXY ( Vec2f np )
{
  changed = true;
  bounds.top = np.y;
  bounds.left = np.x;
}

void Widget::SetXY ( float x, float y )
{
  changed = true;
  bounds.left = x;
  bounds.top = y;
}

Vec2f Widget::GetXY() const
{
  return Vec2f( bounds.left, bounds.top );
}

Vec2f Widget::GetSize() const
{
  return Vec2f( bounds.width , bounds.height );
}

BoxWidget::BoxWidget( sf::String t, Vec2f pos ,
             Vec2f sz , Color c, Color tc , Color oc, float ot ):
  text( t ),
  outline_thickness( ot ),
  text_color( tc ),
  outline_color( oc )
{
  color = c;
  is_active = true;
  bounds = sf::Rect<float>( pos, sz );
  changed = true;
}

void BoxWidget::Draw ( RenderTargetPtr win ) const
{
  win->draw( my_rect );
  win->draw( my_text );
}




void Label::Draw ( RenderTargetPtr win ) const
{
  win->draw( my_text );
}

// void Widget::TriggerHover() 
// {
//   if ( !was_hovered ) {
//     was_hovered = true;
//     for( size_t i = 0; i < hover_events.size(); i++ ) {
//       hover_events[i]( true );
//     }
//   }
// }

// void Widget::TriggerLeaveHover()
// {
//   if ( was_hovered ) {
//     was_hovered = false;
//     for( size_t i = 0; i < hover_events.size(); i++ ) {
//       hover_events[i]( false );
//     }
//   }
// }

// void Widget::TriggerClick()
// {
//   for ( size_t i = 0; i < click_events.size(); i++ ) {
//     click_events[i]();
//   }
//   // Clicked( heal::core::app::);
// }

void Widget::SetKeyboardReady( bool yesno )
{
  keyboard_ready = yesno;
}

bool Widget::IsKeyboardReady() const
{
  return keyboard_ready;
}

bool Widget::KeyboardInput( heal::core::Event const& )
{
  return false;
}

InputField::InputField( sf::String t, Vec2f pos , Vec2f sz,
                        sf::Color col  ):
  BoxWidget( t, pos, sz, col )

{
  this->SetSelectable( true );
  this->SetKeyboardReady( true );
}


bool InputField::KeyboardInput( heal::core::Event const& ev )
{
  if( ev.type == Event::TextEntered )
    {
      switch( ev.text.unicode ){
      case 0x8:
        this->RemoveLastChar();
        break;
      default:
        this->AddChar( ev.text.unicode ) ;
      }
      this->Refresh();
      return true;
    }
  return false;
}



void InputField::AddChar( char c )
{
  my_text.setString(text += c);
}

Button::Button( sf::String t,
                Vec2f pos, Vec2f sz,  Color c, Color hover ):
  BoxWidget( t, pos, sz, c ),
  is_hovered( false ), hover_color( hover ),
  click_action( [](ClickInfo){} )
{
}

void Button::Clicked( ClickInfo c )
{
  click_action( c );
}

void Button::Hovered( bool entered )
{
  is_hovered = entered;
}

void Button::SetClickAction( ClickAction c )
{
  click_action = c;
}

void Button::Refresh()
{
  BoxWidget::Refresh();
  if( is_hovered ){
    my_rect.setFillColor( hover_color );
  }else{
    my_rect.setFillColor( color );
  }
}

void BoxWidget::Refresh()
{
  if ( changed ){
    my_rect.setPosition( Vec2f( bounds.left, bounds.top ));
    my_rect.setSize( Vec2f( bounds.width - (outline_thickness)*2,
                            bounds.height - (outline_thickness)*2));
    my_rect.setFillColor( color );
    my_rect.setOutlineThickness( outline_thickness  );
    my_rect.setOutlineColor( outline_color );
    my_text.setFont( core::default_font );
    my_text.setString( text );
    my_text.setCharacterSize( core::default_font_size );
    Vec2f text_pos ( 0, 0 );

    text_pos.x = (bounds.width * 0.5 + bounds.left)
      - my_text.getGlobalBounds().width * 0.5;
    text_pos.y = (bounds.height * 0.5 + bounds.top)
      - (core::default_font_size * 0.8);
    
    my_text.setPosition( text_pos  );
    changed = false;
  }
  
}

void InputField::RemoveLastChar()
{
  size_t sz = text.getSize();
  if ( sz > 0 )
    text.erase( sz - 1 ); 
}

void InputField::Refresh()
{
  if( changed ){
    BoxWidget::Refresh();
    Vec2f text_pos ( bounds.left + 10 , bounds.top );
    text_pos.y += (bounds.height  * 0.4 ) - core::default_font_size * 0.8;
    sf::String tmp = text;
    float tw  = my_text.getGlobalBounds().width;

    while( tw > bounds.width-25 ){
      tmp.erase(0);
      my_text.setString( tmp );
      tw  = my_text.getGlobalBounds().width;
    }

    my_text.setPosition( text_pos  );
    changed = false;
  }
  
}

void InputField::SetTextReciever( std::function<void(sf::String)> rec )
{
  text_receiver = rec; 
}

void BoxWidget::SetText( sf::String t )
{
  text = t;
  my_text.setString( t );
}

Label::Label( sf::String text, 
              Vec2f pos, Vec2f sz, sf::Color color ):
  BoxWidget( text, pos, sz )
{
  my_text.setFillColor( color );
}

bool BoxWidget::Contains( math::Vec2f const& p ) const 
{
  bounds.contains( p.x, p.y );
}


// void Widget::AddClickEvent( std::function<void()> cb )
// {
//   click_events.push_back( std::move( cb ) );
// }

// void Widget::AddHoverEvent( std::function<void(bool)> cb )
// {
//   hover_events.push_back( cb );
// }

