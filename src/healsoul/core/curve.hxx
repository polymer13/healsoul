#ifndef HEALSOUL_CORE_CURVE_HEADER_HXX
#define HEALSOUL_CORE_CURVE_HEADER_HXX

#include "healsoul/core/core.hxx"

namespace heal
{
  

namespace geom
{
  using Vec2f = sf::Vector2f;
  using namespace core;
  class Curve;
  class CurveIter
  {
    friend class Curve;
    const Curve* my_path;
    float accumulator;
    float t_value;
    Vec2f tmp;
  public:
    CurveIter( const Curve& c, float acc );
    const Vec2f& operator* ();
    CurveIter& operator++(int);
    void Restart();
    float GetT () const;
  };

  class Curve
  {
    Vec2f point_a;
    Vec2f point_b;
    Vec2f point_c;
    
  public:
    Curve( Vec2f a, Vec2f b, Vec2f c );
    Vec2f Along( float t_value ) const;
    CurveIter GetIter() const;
    void SetPointA( Vec2f a );
    void SetPointB( Vec2f b );
    void SetPointC( Vec2f c );
  };

}

}
#endif  //_HEADER_CORE_CURVE_HXX_
