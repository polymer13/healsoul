#include "layout.hxx"
#include <iostream>
#include "debug.hxx"

using namespace heal;
using namespace heal::gui;

// void Layout::SetVisible( size_t index, bool yes = true )
// {
//   children[ index ].SetVisible( yes );
// }

// void Layout::SetVisibleRange( size_t first, size_t last, bool yes = true )
// {
//   for( auto c = children.begin() + first; c < children.begin() + 8; c++ ){
//     c->SetVisible( yes );
//   } 
// }

// void Layout::SetVisibleWithin( gfx::Rect const& rect,  bool yes = true )
// {

// }


void Layout::AddWidget( WidgetPtr w )
{
  children.push_back( w );
}


void Layout::RemoveWidget( WidgetPtr w )
{
  for( auto i = children.begin(); i < children.end(); i++ ){
    if ( *i == w )
      children.erase( i );
  }
}

void Layout::RemoveWidget( size_t index )
{
  children.erase( children.begin() + index );
}

size_t Layout::GetChildCount()
{
  return children.size();
}

void Layout::Clear()
{
  children.clear();
}

VLayout::VLayout( math::Vec2f pos, math::Vec2f size, float off ):
  offset( off )
{
  SetXY( pos );
  SetSize( size );
}

HLayout::HLayout( Vec2f pos, Vec2f size, float off ):
  offset( off )
{
  SetXY( pos );
  SetSize( size );
}

void VLayout::Draw( RenderTargetPtr w ) const 
{
  background_rect.Draw( w );
  for( auto i = children.begin(); i != children.end(); i++ ){
    (*i)->Draw( w );
  }
}

void VLayout::Refresh()
{
  WidgetGroupIterator current = children.begin();
  Vec2f pos = GetXY();
  Vec2f sz = GetSize();
  background_rect.SetSize( pos );
  background_rect.SetXY( sz );
  background_rect.SetBackColor( Color::Black );
  background_rect.Refresh();
  Vec2f unit_size (  sz.x, sz.y/(float)children.size()-offset );

  while ( current != children.end() ){
    (*current)->SetXY( pos );
    (*current)->SetSize( unit_size );
    (*current)->Refresh();
    pos.y += unit_size.y + offset;
    current++;
  }
}

void HLayout::Draw( RenderTargetPtr w ) const 
{
  
}

void HLayout::Refresh()
{
  background_rect.SetSize( GetSize() );
  background_rect.SetXY( GetXY() );

  WidgetGroupIterator current = children.begin();
  Vec2f pos = GetXY();
  Vec2f sz = GetSize();

  Vec2f unit_size ( sz.x, sz.y/children.size());

  while ( current != children.end() ){
    (*current)->SetXY( pos );
    (*current)->SetSize( unit_size );
    pos.y = (*current)->Top();
    pos.x = (*current)->Right();
    current++;
  }
}

bool VLayout::Contains( math::Vec2f const& p ) const
{
  
}

bool HLayout::Contains( math::Vec2f const& p ) const
{
  
}

LayoutPtr gui::make_VLayout( Vec2f const& pos, Vec2f const& size, float off )
{
  return std::make_shared<VLayout>( pos, size, off );
}

void VLayout::SetOffset( float off )
{
  offset = off; 
}

void HLayout::SetOffset( float off )
{
  offset = off; 
}

void Layout::SetBackColor( Color col )
{
  background_rect.SetBackColor( col );
}
