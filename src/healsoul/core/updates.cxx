#include "updates.hxx"

using namespace heal;
using namespace heal::core;

SwapUpdate::SwapUpdate ( Update up ):
  update_call( up )
{
  
}


void SwapUpdate::operator= ( Update up )
{
  update_call = up;
}

void SwapUpdate::operator() ()
{
  update_call();
}
