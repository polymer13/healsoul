#ifndef HEALSOULS_CORE_MATH_HEADER_HXX
#define HEALSOULS_CORE_MATH_HEADER_HXX
#include <SFML/System/Vector2.hpp>
#include <cmath>

namespace  math 
{
  using Vec2f = sf::Vector2f;
  using Vec2i = sf::Vector2i;

  float magnitude ( Vec2f v )
  {
    float x = v.x * v.x;
    float y = v.y * v.y;
    return std::sqrt( x + y );
  }

  float angle_from_origin( Vec2f f )
  {
    return std::acos ( f.y / f.x ); 
  }
}

math::Vec2f operator*( const math::Vec2f& a, float s )
{
  return math::Vec2f( a.x * s, a.y * s );
}

math::Vec2f operator*( float s, const math::Vec2f& a )
{
  return math::Vec2f( a.x * s, a.y * s );
}


#endif // HEALSOULS_CORE_MATH_HEADER_HXX 
