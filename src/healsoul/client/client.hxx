#ifndef MOD_CLIENT_HEADER_HXX
#define MOD_CLIENT_HEADER_HXX
#include <string>
#include <cstdint>
#include <SFML/Network.hpp>

namespace front
{
  typedef uint16_t u16;
  typedef uint32_t u32;

  // client_config
  struct ClientConfig{
    std::string name;
    u16 port;
    std::string destination;
  };

  class Client
  {
    std::string error;
    bool is_broken;
    void prepareSocket();

    
  public:
    sf::TcpSocket socket;
    ClientConfig config;
    explicit Client( ClientConfig cfg );
    bool makeHTTP ( std::string& error );
  };

}


#endif 
