#include "client.hxx"
#include <iostream>
#include <cstdint>
#include <cstring>
#include <fstream>

using namespace front;
Client::Client( ClientConfig cfg ):
  config( cfg ),
  is_broken ( false )
{
  std::cout << "Constructing client ... \n";
  prepareSocket();
  if ( is_broken ) 
    std::cout << "ERROR: " << error;
}

void Client::prepareSocket( )
{
  sf::Socket::Status status
    = socket.connect ( config.destination, config.port );

  if ( status != sf::Socket::Done ) {
    is_broken = true;
    error = "Could not connect Socket to destination:\n\t\t" +
      config.destination;
    return;
  }
  // If OK |
  //       V
  return;
}

int main ( void )
{
  std::string content;
  content.reserve( 10000 );
  std::cout << "Hello, there!\n";
  ClientConfig cfg;
  cfg.destination = "www.gnu.org";
  cfg.port = 80; 
  cfg.name = "hello"; 
  const char* request = "GET /graphics/heckert_gnu.transp.small.png HTTP/1.1\r\nHost: www.gnu.org\r\n\r\n";
  char buffer[101] = {0};
  size_t received =  0;

  Client c1 ( cfg );
  if( c1.socket.send( request, strlen(request)) != sf::Socket::Done ) {
    std::cout << "Could not send HTTP request!\n";
  }else {
    int crlf = 0;
    bool reading_headers = false;
    while (true) {
      if( c1.socket.receive( buffer , 100, received ) != sf::Socket::Done ) {
        break;
      }{
        char* idx = NULL;
        if ( received > 0 ) {
          buffer[received-1] = 0;
          content.append(buffer);
        }
      }
    }
  }
  std::cout << "Writing file: index.html\n";
  std::fstream f1 ( "hello.png", std::ios::out );
  size_t found = content.find("\r\n\r\n");
  f1.write( content.c_str()+found+4, content.length()+found-1-3 );
  f1.flush();
  f1.close();
  return 0;
}
