#version 120

uniform sampler2D texture_sampler;
varying vec2 UV;
varying vec3 frag_pos; 
varying vec3 normal;
uniform float specular_intensity = 0.8;
uniform float diffuse_intensity = 0.4;
uniform float ambient_intensity = 0.1;
uniform vec3 light_color = vec3(1.0, 1.0, 1.0);

#define NOL 2
// uniform vec3[NOL] light_positions = vec3[NOL]( vec3( 0.0, 5.0f, 0.0 ), 
//                                            vec3( 0.0, 0.0, 10.0 ));

uniform vec3 light_pos = vec3( 0.0, 5.0, 0.0 );
uniform int number_of_lights = NOL;
uniform vec3 view_pos;

void main () {

  vec2 uv = UV;
  vec4 color =  vec4(0.5,0.0,0.9,1.0);
  // vec4 color =  texture2D( texture_sampler, uv );

  vec3 diffuse, specular, ambient = light_color * ambient_intensity;
  vec3 view_dir = normalize( view_pos - frag_pos  );
  vec3 light_dir = normalize( light_pos - frag_pos );
  vec3 norm = normalize( normal );

  diffuse = light_color * clamp( dot( light_dir, norm), 0.0, 1.0 );
  vec3 reflect_dir = reflect( -light_dir, norm );
  specular = light_color * pow( max( dot( view_dir,
                                          reflect_dir),
                                     0.0) ,
                                2.0 );

  specular += 1.8 * light_color * pow( max( dot( view_dir,
                                             reflect_dir),
                                        0.0) ,
                                   256.0 );


  diffuse *= diffuse_intensity;
  specular *= specular_intensity;

  gl_FragColor = vec4(ambient + diffuse + specular,0.0) * color;

  // gl_FragColor = vec4( sin(gl_FragCoord.x), 0.0, 0.0, 1.0 );
}
