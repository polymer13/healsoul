#version 130

attribute vec3 vertex_position_modelspace;
attribute vec2 vertex_uv;
attribute vec3 vertex_normal;

uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 proj_mat;
uniform mat3 normal_mat;
uniform float delta;

varying vec2 UV;
varying vec3 frag_pos;
varying  vec3 normal;

void main () {
  UV = vertex_uv;
  mat4 MVP = proj_mat * view_mat * model_mat;
  frag_pos = vec3(model_mat * vec4(vertex_position_modelspace, 1.0));
  normal = normal_mat *  normalize( vertex_normal );
  // Passing transformed vertecies
  vec4 pos = MVP *  vec4( vertex_position_modelspace, 1.0 );
  gl_Position = pos;
}
